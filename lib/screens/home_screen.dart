import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:rss_app/bloc/movie_bloc.dart';
import 'package:rss_app/resources/app_text.dart';
import 'package:rss_app/screens/favorite_screen.dart';
import 'package:rss_app/theme.dart';
import 'package:rss_app/widgets/item_widget.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  @override
  Widget build(BuildContext context) {
    FutureOr onGoBack(dynamic value) {
      BlocProvider.of<MoviesBloc>(context).add(LoadMoviesEvent());
    }

    return BlocBuilder<MoviesBloc, MoviesState>(builder: (context, state) {
      if (state is Loaded) {
        return Scaffold(
          backgroundColor: greyColor,
          appBar: AppBar(
            elevation: 1,
            centerTitle: true,
            title: Text(
              movies,
              style: Theme.of(context).textTheme.headline1,
            ),
            backgroundColor: Colors.white,
            actions: [
              IconButton(
                  onPressed: () => Navigator.of(context).push(MaterialPageRoute(builder: (context) => const FavoriteScreen())).then(onGoBack),
                  icon: const Icon(Icons.favorite_border_outlined, color: pinkColor))
            ],
          ),
          body: ListView.builder(
            itemCount: state.movies.length,
            itemBuilder: (ctx, i) => ItemWidget(movie: state.movies[i]),
          ),
        );
      } else {
        return const Center(
          child: CircularProgressIndicator(),
        );
      }
    });
  }
}
