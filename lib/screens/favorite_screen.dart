import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:rss_app/bloc/movie_bloc.dart';
import 'package:rss_app/resources/app_text.dart';
import 'package:rss_app/theme.dart';

class FavoriteScreen extends StatelessWidget {
  const FavoriteScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    context.read<MoviesBloc>().add(ShowFavoriteMoviesEvent());
    return BlocBuilder<MoviesBloc, MoviesState>(
      builder: (context, state) {
        return Container(
          color: Colors.white,
          child: SafeArea(
            child: Scaffold(
              backgroundColor: Colors.white,
              appBar: AppBar(
                elevation: 0,
                title: Text(
                  favorite,
                  style: Theme.of(context).textTheme.headline1,
                ),
                backgroundColor: Colors.white,
                leading: IconButton(
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                  icon: const Icon(
                    Icons.arrow_back_ios,
                    color: pinkColor,
                  ),
                ),
              ),
              body: (state is Empty)
                  ? Container(
                      padding: const EdgeInsets.symmetric(horizontal: 32),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Image.asset(
                            "assets/images/awkward.png",
                            width: 160,
                          ),
                          const SizedBox(
                            height: 16,
                          ),
                          Text(
                            emptyTitle,
                            textAlign: TextAlign.center,
                            style: Theme.of(context).textTheme.headline2,
                          ),
                          const SizedBox(
                            height: 8,
                          ),
                          Text(
                            emptySubtitle,
                            textAlign: TextAlign.center,
                            style: Theme.of(context).textTheme.bodyText1,
                          ),
                          const SizedBox(
                            height: 16,
                          ),
                          OutlinedButton(
                            onPressed: () {
                              Navigator.of(context).pop();
                            },
                            child: Text(
                              goToCatalog,
                              style: Theme.of(context).textTheme.button
                            ),
                            style: OutlinedButton.styleFrom(
                              backgroundColor: pinkColor,
                              padding: const EdgeInsets.symmetric(
                                horizontal: 48,
                                vertical: 16,
                              ),
                            ),
                          ),
                        ],
                      ),
                    )
                  : (state is ShowFavorite)
                      ? GridView.count(
                          crossAxisCount: 2,
                          children: state.movies
                              .map((item) => Container(
                                    padding: const EdgeInsets.symmetric(horizontal: 12, vertical: 10),
                                    child: Stack(
                                      children: [
                                        Positioned(
                                            right: 0,
                                            child: IconButton(
                                              onPressed: () => BlocProvider.of<MoviesBloc>(context).add(RemoveFromFavoriteEvent(movie: item)),
                                              icon: const Icon(Icons.favorite, size: 22, color: pinkColor),
                                            )),
                                        Positioned.fill(
                                          top: 10,
                                          child: Column(
                                            children: [
                                              Expanded(child: Image.asset(item.icon)),
                                              const SizedBox(
                                                height: 10,
                                              ),
                                              Text(
                                                item.name,
                                                textAlign: TextAlign.center,
                                                style: Theme.of(context).textTheme.bodyText2
                                              )
                                            ],
                                          ),
                                        ),
                                      ],
                                    ),
                                  ))
                              .toList(),
                        )
                      : const Center(
                          child: CircularProgressIndicator(),
                        ),
            ),
          ),
        );
      },
    );
  }
}
