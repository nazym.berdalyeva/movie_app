import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'package:rss_app/bloc/movie_bloc.dart';
import 'package:rss_app/resources/app_text.dart';
import 'package:rss_app/screens/home_screen.dart';
import 'package:rss_app/theme.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider<MoviesBloc>(
        create: (context) => MoviesBloc()..add(LoadMoviesEvent()),
        child: MaterialApp(
          title: movies,
          theme: kTheme,
          home: const HomeScreen(),
        ));
  }
}
