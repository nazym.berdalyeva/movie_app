part of 'movie_bloc.dart';


abstract class MoviesState extends Equatable {
  const MoviesState();
}

class Loading extends MoviesState {
  @override
  List<Object> get props => [];
}
class Failed extends MoviesState {
  @override
  List<Object> get props => [];
}

class Empty extends MoviesState {
  @override
  List<Object> get props => [];
}

class Loaded extends MoviesState {
  final List<Movie> movies;

  const Loaded({required this.movies});

  @override
  List<Object> get props => [movies];
}

class ShowFavorite extends MoviesState {
  final Set<Movie> movies;

  const ShowFavorite({required this.movies});

  @override
  List<Object> get props => [movies];
}