import 'package:bloc/bloc.dart';
import 'package:rss_app/data/data_provider.dart';
import 'package:equatable/equatable.dart';

part 'movies_event.dart';

part 'movies_state.dart';

class MoviesBloc extends Bloc<MoviesEvent, MoviesState> {
  MoviesBloc() : super(Loading()) {
    on<LoadMoviesEvent>(_loadMovies);
    on<UpdateFavoriteMoviesEvent>(_updateFavoritesMovies);
    on<ShowFavoriteMoviesEvent>(_showFavoritesMovies);
    on<RemoveFromFavoriteEvent>(_removeFromFavorite);
  }

  final DataProvider dataProvider = DataProvider();

  void _loadMovies(LoadMoviesEvent event, Emitter<MoviesState> emit) async {
    emit(Loading());
    var list = dataProvider.getMovies();
    list.forEach((element) {
      element.favorite = dataProvider.favorites.contains(element);
    });
    emit(Loaded(movies: list));
  }

  void _updateFavoritesMovies(UpdateFavoriteMoviesEvent event, Emitter<MoviesState> emit) async {
    emit(Loading());
    if (!event.movie.favorite) {
      event.movie.favorite = true;
      dataProvider.movies[event.movie.id].favorite = true;
      dataProvider.favorites.add(event.movie);
    } else {
      event.movie.favorite = false;
      dataProvider.favorites.remove(event.movie);
      dataProvider.movies[event.movie.id].favorite = false;
    }
    emit(Loaded(movies: dataProvider.movies));
  }

  void _showFavoritesMovies(ShowFavoriteMoviesEvent event, Emitter<MoviesState> emit) async {
    if (dataProvider.favorites.isEmpty) {
      emit(Empty());
    } else {
      emit(ShowFavorite(movies: dataProvider.favorites));
    }
  }

  void _removeFromFavorite(RemoveFromFavoriteEvent event, Emitter<MoviesState> emit) async {
    emit(Loading());
    event.movie.favorite = false;
    dataProvider.favorites.remove(event.movie);
    dataProvider.movies[event.movie.id].favorite = false;
    add(ShowFavoriteMoviesEvent());
  }
}
