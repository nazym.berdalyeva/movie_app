part of 'movie_bloc.dart';

abstract class MoviesEvent extends Equatable{
  const MoviesEvent();
}

class LoadMoviesEvent extends MoviesEvent {
  @override
  List<Object> get props => [];
}

class ShowFavoriteMoviesEvent extends MoviesEvent {
  @override
  List<Object> get props => [];
}

class RemoveFromFavoriteEvent extends MoviesEvent {
  final Movie movie;

  const RemoveFromFavoriteEvent({required this.movie});

  @override
  List<Object> get props => [movie];
}

class UpdateFavoriteMoviesEvent extends MoviesEvent {
  final Movie movie;

  const UpdateFavoriteMoviesEvent({required this.movie});

  @override
  List<Object> get props => [movie];
}

//class AddToFavorite extends MoviesEvent {
//  final Movie movie;
//
//  AddToFavorite({required this.movie});
//}

