import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:rss_app/bloc/movie_bloc.dart';
import 'package:rss_app/data/data_provider.dart';
import 'package:rss_app/theme.dart';

class ItemWidget extends StatelessWidget {
  final Movie movie;

  const ItemWidget({
    Key? key,
    required this.movie,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<MoviesBloc, MoviesState>(
      builder: (context, state) {
        return Container(
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(12),
          ),
          margin: const EdgeInsets.all(12.0),
          child: Row(
            children: [
              Container(
                height: 66,
                width: 76,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(18),
                ),
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(18),
                  child: Image.asset(
                    movie.icon,
                    fit: BoxFit.fill,
                  ),
                ),
              ),
              const SizedBox(
                width: 12,
              ),
              Text(movie.name, style: Theme.of(context).textTheme.bodyText1,),
              const Spacer(flex: 2),
              IconButton(
                onPressed: () {
                  BlocProvider.of<MoviesBloc>(context).add(UpdateFavoriteMoviesEvent(movie: movie));
                },
                icon: (movie.favorite)
                    ? const Icon(
                        Icons.favorite,
                        color: pinkColor,
                      )
                    : const Icon(
                        Icons.favorite_border,
                        color: pinkColor,
                      ),
              )
            ],
          ),
        );
      },
    );
  }
}
