const String movies = "Фильмы";
const String favorite = "Избранное";
const String emptyTitle = "Здесь пока ничего нет";
const String emptySubtitle = "Добавляйте любимые фильмы в избранное чтобы не искать их снова";
const String goToCatalog = "Перейти в каталог";