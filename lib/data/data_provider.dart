class Movie {
  final int id;
  final String name;
  final String icon;
  bool favorite = false;

  Movie({required this.id, required this.icon, required this.name});
}

class DataProvider {
  List<Movie> movies = [];
  final favorites = Set<Movie>();

  final movieTitles = [
    "Мавританец",
    "Круиз по джунглям",
    "Гудбай, Америка",
    "Довод",
    "Душа (2020)",
    "Еще по одной",
    "Огонь (2020)",
    "Зависнуть в Палм-Спрингс",
    "Калашников",
    "Семейка Крудс: Новоселье",
    "Зов предков (2020)",
    "Красный призрак",
    "Рыцари справедливости",
    "Хэппи-энд (2020)",
    "Доктор Лиза",
    "Земля кочевников",
    "Мастер меча",
    "Серебряные коньки",
  ];

  List<Movie> getMovies() {
    if (movies.isEmpty) {
      movies = List<Movie>.generate(18, (i) => Movie(id: i, name: movieTitles[i], icon: "assets/images/movie$i.jpg"));
    }
    return movies;
  }
}
