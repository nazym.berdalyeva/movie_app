import 'package:flutter/material.dart';

final ThemeData kTheme = _buildTheme();

ThemeData _buildTheme() {
  final ThemeData base = ThemeData.light();

  return base.copyWith(
    textTheme: const TextTheme(
      headline1: TextStyle(fontStyle: FontStyle.normal, fontWeight: FontWeight.w700, fontSize: 20.0, color: darkTitleColor,),
      headline2: TextStyle(
        fontStyle: FontStyle.normal,
        fontSize: 18,
        fontWeight: FontWeight.w800,
        color: darkTitleColor,
      ),
      bodyText1: TextStyle(
        fontStyle: FontStyle.normal,
        fontSize: 16,
        fontWeight: FontWeight.w400,
        color: darkTitleColor,
      ),
      button: TextStyle(
        fontStyle: FontStyle.normal,
        fontSize: 16,
        fontWeight: FontWeight.w400,
        color: Colors.white
      ),
      bodyText2: TextStyle(
        fontStyle: FontStyle.normal,
        fontSize: 14,
        fontWeight: FontWeight.w400,
        color: darkTitleColor,
      ),
    ),
  );
}

const Color greyColor = Color.fromRGBO(207, 211, 219, 1);
const Color pinkColor = Color.fromRGBO(240, 107, 111, 1);
const Color darkTitleColor = Color.fromRGBO(55, 68, 91, 1);
